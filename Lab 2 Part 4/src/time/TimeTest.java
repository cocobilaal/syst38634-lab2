package time;

import static org.junit.Assert.*;

import org.junit.Test;

import test.Time;


public class TimeTest{
	
	//********************************/ getTotalMillseconds()********************************//
	
	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMillseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue ("Invalid number of milliseconds", totalMillseconds == 5);
	}
	
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test 
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of millseconds");
	}
	
	//********************************/ getTotalSeconds() /********************************//
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		/*int totalSeconds = */ Time.getTotalSeconds("01:01:0a");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The time provided does not match the result", totalSeconds == 3719);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3720);
	}
	
}

